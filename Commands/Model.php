<?php
namespace App\Commands;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class Model extends BaseCommand
{
    protected $group       = 'Custom Commands';
    protected $name        = 'make:model';
    protected $description = 'Make a model file.';
    protected $usage       = 'make:model [name]';
    protected $arguments   = ['name' => 'The model name'];
    protected $options     = [];

    public function run(array $params)
    {
        $fileName = array_shift($params);
        $response = $this->Model($fileName);
        CLI::write($response);
    }

    protected function Model($fileName)
    {
        $path = $this->FolderCreator($fileName, 'Models');
        // File exist.
        if (file_exists(APPPATH . $path . '.php')) {
            return "This model file already exists.\n";
        }

        // Actually write file.
        if (!write_file(APPPATH . $path . '.php',
            $this->ModelCreator($path))) {
            return "Unable to write the file.\n";
        }
        return $path . " model was created!\n";

    }

    protected function ModelCreator($fileName)
    {
        $path     = $fileName;
        $fileName = explode('/', $fileName)[count(explode('/', $fileName)) - 1]; // Find the end of array
        $data     = $this->Template($path, $fileName);
        return $data;
    }

    protected function FolderCreator($fileName)
    {
        $folder = APPPATH . 'Models/';

        $arrDir = explode('.', $fileName);

        unset($arrDir[count($arrDir) - 1]);

        foreach ($arrDir as $key => $value) {

            $folder .= $value . '/';

            if (!file_exists($folder)) {

                mkdir($folder);

            }
        }

        $arrDir = explode('.', $fileName);

        return 'Models/' . implode('/', $arrDir);
    }

    protected function Template($path, $name)
    {
        $path     = substr($path, 0, strrpos($path, '/'));
        $path     = str_replace('/', '\\', $path);
        $symbol   = '$';
        $template = <<<EOD
<?php namespace App\\$path;

use CodeIgniter\Model;

class {name} extends Model
{
    protected $symbol{space}table = '';

    protected $symbol{space}primaryKey = '';

    protected $symbol{space}allowFields = [];

    protected $symbol{space}useTimestamps = true;

    protected $symbol{space}createdField = 'created_at';
    protected $symbol{space}updatedField = 'updated_at';
    protected $symbol{space}deletedField = 'deleted_at';
    protected $symbol{space}statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $symbol{space}returnType = 'object';

    protected $symbol{space}skipValidation     = false;

    public $symbol{space}validationRules = [
        'name' => 'required',
    ];

    public $symbol{space}validationMessages = [
        //Message Example
        'name' => [
            'required' => 'Name is required.'
        ],
    ];
}

EOD;

        $template = str_replace('{name}', $name, $template);
        $template = str_replace('{space}', '', $template);
        return $template;
    }
}
