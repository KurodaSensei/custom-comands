<?php
namespace App\Commands;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class Controller extends BaseCommand
{
    protected $group       = 'Custom Commands';
    protected $name        = 'make:controller';
    protected $description = 'Make a controller file.';
    protected $usage       = 'make:controler [name]';
    protected $arguments   = ['name' => 'The controller name'];
    protected $options     = ['--resource' => 'Set resource the controller', '-r' => 'Set resource the controller'];

    public function run(array $params)
    {
        $fileName = array_shift($params);
        $resource = CLI::getOption('r') ?? CLI::getOption('resource');
        $response = $this->Controller($fileName, $resource);
        CLI::write($response);
    }

    protected function Controller($fileName, $resource)
    {
        $path = $this->FolderCreator($fileName, 'Controllers');
        // File exist.
        if (file_exists(APPPATH . $path . '.php')) {
            return "This controller file already exists.\n";
        }

        // Actually write file.
        if (!write_file(APPPATH . $path . '.php',
            $this->ControllerCreator($path, $resource))) {
            return "Unable to write the file.\n";
        }
        return $path . " controller was created!\n";

    }

    protected function ControllerCreator($fileName, $resource)
    {
        $path     = $fileName;
        $fileName = explode('/', $fileName)[count(explode('/', $fileName)) - 1]; // Find the end of array
        $data     = $this->Template($path, $fileName, $resource);
        return $data;
    }

    protected function FolderCreator($fileName)
    {
        $folder = APPPATH . 'Controllers/';

        $arrDir = explode('.', $fileName);

        unset($arrDir[count($arrDir) - 1]);

        foreach ($arrDir as $key => $value) {

            $folder .= $value . '/';

            if (!file_exists($folder)) {

                mkdir($folder);

            }
        }

        $arrDir = explode('.', $fileName);

        return 'Controllers/' . implode('/', $arrDir);
    }

    protected function Template($path, $name, $resource)
    {
        if (is_null($resource)) {
            $path     = substr($path, 0, strrpos($path, '/'));
            $path     = str_replace('/', '\\', $path);
            $template = <<<EOD
<?php namespace App\\$path;

use App\Controllers\BaseController;

class {name} extends BaseController
{
    public function __construct(){
        parent::__construct();
    }
}

EOD;
        } else {
            $path     = substr($path, 0, strrpos($path, '/'));
            $path     = str_replace('/', '\\', $path);
            $template = <<<EOD
<?php namespace App\\$path;

use App\Controllers\BaseController;

class {name} extends BaseController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
    }

    public function show(){
    }

    public function create(){
    }

    public function update(){
    }

    public function delete(){
    }
}

EOD;
        }

        $template = str_replace('{name}', $name, $template);
        return $template;
    }
}
